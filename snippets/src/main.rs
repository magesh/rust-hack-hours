use std::fs::OpenOptions;
use std::io::{self, BufWriter, SeekFrom, Seek};

fn main() -> io::Result<()> {
    let mut file = OpenOptions::new()
        .append(true)
        .open("/tmp/words")?;
    println!("{}", file.seek(SeekFrom::Current(0))?);

    Ok(())
}
