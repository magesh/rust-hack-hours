// ref. https://uva.onlinejudge.org/external/102/10260.pdf
use std::io::{self, BufRead};
// use std::collections::HashMap;


fn main() -> io::Result<()> {
    let codes = vec![
        0, // A
        1, // B
        2, // C
        3, // D
        0, // E
        1, // F
        2, // G
        0, // H
        0, // I
        2, // J
        2, // K
        4, // L
        5, // M
        5, // N
        0, // O
        1, // P
        2, // Q
        6, // R
        2, // S
        3, // T
        0, // U
        1, // V
        0, // W
        2, // X
        0, // Y
        2, // Z
    ];

    for name in io::stdin().lock().lines() {
        let name = name?;
        let mut prev_code = 0;

        for c in name.bytes() {
            let cur_code = codes[(c - b'A') as usize];
            if cur_code > 0 && cur_code != prev_code {
                print!("{}", cur_code);
            }
            prev_code = cur_code;
        }
        println!();
    }

    Ok(())
}
