use std::collections::HashMap;
use std::error::Error;
use std::io::{self, Read};

fn main() -> Result<(), Box<dyn Error>> {
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    handle.read_to_string(&mut buffer)?;
    let intcodes: Vec<usize> = buffer
        .trim()
        .split(',')
        .map(|x| x.parse::<usize>().unwrap())
        .collect();

    let mut result = HashMap::new();
    for (i, c) in intcodes.chunks(4).enumerate() {
        let op = *(result.get(&(i * 4)).unwrap_or(&c[0]));
        if op == 99 {
            break;
        }

        assert!(op == 1 || op == 2, format!("op code {} not in [1, 2]", op));
        result.insert(
            c[3],
            if op == 1 {
                result.get(&c[1]).unwrap_or(&intcodes[c[1]])
                    + result.get(&c[2]).unwrap_or(&intcodes[c[2]])
            } else if op == 2 {
                result.get(&c[1]).unwrap_or(&intcodes[c[1]])
                    * result.get(&c[2]).unwrap_or(&intcodes[c[2]])
            } else {
                0
            },
        );
    }

    for (chunk_i, chunk) in intcodes.chunks(4).enumerate() {
        for (i, v) in chunk.iter().enumerate() {
            let i = i + chunk_i * 4;
            print!("{},", result.get(&i).unwrap_or(v));
        }
        println!();
    }
    Ok(())
}
