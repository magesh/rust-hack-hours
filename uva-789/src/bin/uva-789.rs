use std::io::{self, BufRead};
use std::collections::BTreeMap;

// ref. https://uva.onlinejudge.org/external/7/789.pdf

fn main() -> io::Result<()> {
    let stdin = io::stdin();
    let mut lines = stdin.lock().lines();
    let key = lines.next().expect("no input")?;

    let mut index = BTreeMap::new();
    for (i, each_line) in lines.enumerate() {
        // 1. iterate over each word in each line
        // 2. ignore any char outside the expected range)
        // 3. if word starts with key, store it in the tree along with line number
        for word in each_line?.split_whitespace() {
            let word = word.trim_matches(|c| c < 'A' || c > 'Z');
            if word.starts_with(&key) {
                index.entry(String::from(word))
                    .or_insert_with(Vec::new)
                    .push(i + 1);
            }
        }
    }

    // iterate over tree and print word, and all lines for each word
    for (k, v) in index {
        print!("{}", k);
        for p in v {
            print!(" {}", p);
        }
        println!();
    }

    Ok(())
}
