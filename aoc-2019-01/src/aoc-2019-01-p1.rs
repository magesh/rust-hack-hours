use std::io::{self, BufRead};
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let stdin = io::stdin();
    let mut sum: i32 = 0;
    for line in stdin.lock().lines() {
        let line = line?;
        sum += line.parse::<i32>()? / 3 - 2;
    }
    println!("{}", sum);

    Ok(())
}
