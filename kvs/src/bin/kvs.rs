use failure::format_err;
use kvs::KvStore;
use std::path::Path;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Opt {
    #[structopt(short = "V", long = "version")]
    version: bool,

    #[structopt(subcommand)]
    command: Option<Command>,
}

#[derive(Debug, StructOpt)]
enum Command {
    #[structopt(name = "set")]
    Set { key: String, val: String },

    #[structopt(name = "get")]
    Get { key: String },

    #[structopt(name = "rm")]
    Rm { key: String },
}

fn handle_command(command: Command) -> kvs::Result<()> {
    let mut kvs = KvStore::open(Path::new("."))?;
    match command {
        Command::Set { key, val } => kvs.set(key, val),
        Command::Get { key } => {
            if let Some(val) = kvs.get(key)? {
                println!("{}", val)
            }
            Ok(())
        }
        Command::Rm { key } => kvs.remove(key),
    }
}

fn main() -> kvs::Result<()> {
    let opt = Opt::from_args();

    if opt.version {
        println!(env!("CARGO_PKG_VERSION"));
        Ok(())
    } else {
        match opt.command {
            Some(command) => handle_command(command),
            None => Err(format_err!("no options provided")),
        }
    }
}
