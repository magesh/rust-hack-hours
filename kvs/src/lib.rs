#![deny(missing_docs)]

//! a simple file based fast key value store library

use failure::{format_err, Error};
use serde::{Deserialize, Serialize};
use std::cell::RefCell;
use std::collections::HashMap;
use std::fs::{rename, File, OpenOptions};
use std::io::{BufRead, BufReader, BufWriter, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

/// result type for this module
pub type Result<T> = std::result::Result<T, Error>;

/// struct to manage the key value store
#[derive(Debug)]
pub struct KvStore {
    working_dir: PathBuf,
    index: HashMap<String, u64>,
    wal_writer: BufWriter<File>,
    wal_reader: RefCell<BufReader<File>>,
    wal_count: usize,
}

#[derive(Serialize, Deserialize)]
enum Command {
    #[serde(rename = "set")]
    Set { key: String, val: String },

    #[serde(rename = "del")]
    Del(String),
}

impl KvStore {
    /// creates a KvStore instance given a working directory path
    pub fn open(working_dir: &Path) -> Result<KvStore> {
        let wal = working_dir.join("kvs.wal");

        Ok(KvStore {
            working_dir: working_dir.to_path_buf(),
            index: {
                let index_file = working_dir.join("kvs.idx");
                if index_file.exists() {
                    let file = File::open(&index_file)?;
                    serde_json::from_reader(BufReader::new(file))?
                } else {
                    HashMap::new()
                }
            },
            wal_writer: {
                let file = OpenOptions::new().create(true).append(true).open(&wal)?;
                BufWriter::new(file)
            },
            wal_reader: {
                let file = File::open(&wal)?;
                RefCell::new(BufReader::new(file))
            },
            wal_count: {
                if (&wal).exists() {
                    let file = File::open(&wal)?;
                    BufReader::new(file).lines().count()
                } else {
                    0
                }
            },
        })
    }

    /// sets the value for key (overwriting it)
    pub fn set(&mut self, key: String, val: String) -> Result<()> {
        {
            let loc = self.wal_writer.seek(SeekFrom::End(0))?;
            serde_json::to_writer(
                &mut self.wal_writer,
                &Command::Set {
                    key: key.clone(),
                    val,
                },
            )?;
            self.wal_writer.write_all(b"\n")?;
            self.wal_writer.flush()?;
            self.wal_count += 1;
            self.index.insert(key, loc);
        }

        // compact index occasionally
        if (self.index.len() as f64 / self.wal_count as f64) < 0.5 {
            self.compact()?;
        }

        Ok(())
    }

    /// gets the value of key
    pub fn get(&self, key: String) -> Result<Option<String>> {
        if let Some(&loc) = self.index.get(&key) {
            let mut buf = String::new();
            self.wal_reader.borrow_mut().seek(SeekFrom::Start(loc))?;
            self.wal_reader.borrow_mut().read_line(&mut buf)?;
            if let Command::Set { val, .. } = serde_json::from_str(&buf)? {
                Ok(Some(val.clone()))
            } else {
                Err(format_err!("KvStore::get: Key not found: {}", key))
            }
        } else {
            println!("Key not found");
            Ok(None)
        }
    }

    /// removes key from the store
    pub fn remove(&mut self, key: String) -> Result<()> {
        if self.index.contains_key(&key) {
            serde_json::to_writer(&mut self.wal_writer, &Command::Del(key.clone()))?;
            self.wal_writer.write_all(b"\n")?;
            self.index.remove(&key);
            self.wal_count += 1;
            Ok(())
        } else {
            println!("Key not found");
            Err(format_err!("KvStore::remove: Key not found: {}", key))
        }
    }

    fn compact(&mut self) -> Result<()> {
        let tmp_file = self.working_dir.join("kvs.cpt");
        {
            let mut writer = BufWriter::new(File::create(&tmp_file)?);

            let mut new_index: HashMap<String, u64> = HashMap::new();
            for (key, old_loc) in &self.index {
                let mut buf = String::new();
                self.wal_reader
                    .borrow_mut()
                    .seek(SeekFrom::Start(*old_loc))?;
                self.wal_reader.borrow_mut().read_line(&mut buf)?;

                let new_loc = writer.seek(SeekFrom::Current(0))?;
                new_index.insert(key.to_owned(), new_loc);
                writer.write_all(buf.as_bytes())?;
            }
            self.index = new_index;
            self.wal_count = self.index.len();
        }

        let wal = self.working_dir.join("kvs.wal");
        rename(&tmp_file, &wal)?;

        // reopen files after compaction
        self.wal_writer = {
            let file = OpenOptions::new().create(true).append(true).open(&wal)?;
            BufWriter::new(file)
        };
        self.wal_reader = {
            let file = File::open(&wal)?;
            RefCell::new(BufReader::new(file))
        };

        Ok(())
    }
}

impl Drop for KvStore {
    fn drop(&mut self) {
        let index_file = self.working_dir.join("kvs.idx");
        let file = OpenOptions::new()
            .write(true)
            .truncate(true)
            .create(true)
            .open(&index_file)
            .expect("KvStore::drop: failed to open index file");
        let mut writer = BufWriter::new(file);
        serde_json::to_writer(&mut writer, &self.index)
            .expect("KvStore::drop: failed to save index file");
    }
}
