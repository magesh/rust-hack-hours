use std::io::{self, Result, BufRead};

// uva problem: https://www.udebug.com/UVa/12015

fn main() -> Result<()> {
    let stdin = io::stdin();

    let mut sites = Vec::<String>::new();
    let mut highest = 0;

    // iterate over stdin lines, with line index
    for (index, each_line) in stdin.lock().lines().enumerate() {
        let each_line = each_line?;
        if index == 0 { continue };  // skip test cases count line

        let tokens: Vec<&str> = each_line.split_whitespace().collect();
        let (cur_site, relevance) = (
            tokens[0],
            i32::from_str_radix(tokens[1], 10)
                .expect(&format!("error parsing relevance field in line {}", each_line))
        );

        if relevance > highest {
            highest = relevance;
            sites = vec![cur_site.to_string()];
        } else if relevance == highest {
            sites.push(cur_site.to_string());
        }

        // print result for each case
        if index % 10 == 0 {
            println!("Case #{}:", index / 10);
            for each_site in sites.drain(..) {
                println!("{}", each_site);
            }

            highest = 0;
        }
    }

    Ok(())
}
