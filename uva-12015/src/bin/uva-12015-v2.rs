use std::io::{self, Result, BufRead};

// uva problem: https://www.udebug.com/UVa/12015

fn main() -> Result<()> {
    let stdin = io::stdin();
    let mut iter = stdin.lock().lines();

    // parse number of test cases
    let ncases = iter.next().expect("no input")?;
    let ncases = ncases
        .trim()
        .parse::<i32>()
        .expect(&format!("error parsing number of test cases from {:?}", ncases));

    let mut line_number = 0;
    for case_index in 0..ncases {
        let mut highest = 0;
        let mut top_sites = Vec::new();

        // iterate over stdin lines, 10 lines at a time
        for each_line in (&mut iter).take(10) {
            line_number += 1;
            let each_line = each_line?;

            let mut token_iter = each_line.split_whitespace();
            let site = token_iter
                .next()
                .expect(&format!("error getting site from line {}", line_number));
            let relevance = token_iter
                .next()
                .expect(&format!("error getting relevance from line {}", line_number))
                .parse::<i32>().expect(&format!("error parsing relevance on line {:?}", each_line));

            if relevance > highest {
                highest = relevance;
                top_sites.clear();
                top_sites.push(String::from(site));
            } else if relevance == highest {
                top_sites.push(String::from(site));
            }
        }

        // print result for each case
        println!("Case #{}:", case_index + 1);
        for each_site in top_sites {
            println!("{}", each_site);
        }
    }

    Ok(())
}
